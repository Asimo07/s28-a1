fetch('https://jsonplaceholder.typicode.com/todos/').then((response) => response.json()).then((json) => console.log(json));

async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/todos/');
	let json = await result.json()
	var map = [];
	map = json.map(item => item.title);
	console.log(map);
}
fetchData();

fetch('https://jsonplaceholder.typicode.com/todos/1').then((response) => response.json()).then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		title: 'Created To Do List item',
		userID: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structured",
		id: 1,
		title: 'Updated todo list',
		userID: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structured",
		id: 1,
		title: 'Updated todo list',
		userID: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "07/09/21",
		id: 1,
		status: "Complete",
		title: 'delectus aut autem',
		userID: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE',
	});	